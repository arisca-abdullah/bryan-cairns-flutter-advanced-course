import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Title here'),
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Text('Text here'),
              new Container(
                padding: new EdgeInsets.all(32.0),
                child: new Image(image: new AssetImage('images/avatar.png')),
                decoration: new BoxDecoration(
                  border: new Border.all(color: Colors.grey.shade200, width: 2.0),
                  gradient: new RadialGradient(colors: <Color>[Colors.grey.shade400, Colors.grey.shade50])
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}