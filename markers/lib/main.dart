import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    List<Marker> markers = <Marker>[
      new Marker(
        width: 80.0,
        height: 80.0,
        point: new LatLng(41.8781, -87.6398),
        builder: (context) => new Icon(Icons.pin_drop, color: Colors.red)
      ),
      new Marker(
        width: 80.0,
        height: 80.0,
        point: new LatLng(42.3314, -83.0458),
        builder: (context) => new Icon(Icons.pin_drop, color: Colors.red)
      ),
      new Marker(
        width: 80.0,
        height: 80.0,
        point: new LatLng(42.7325, -84.5555),
        builder: (context) => new Icon(Icons.pin_drop, color: Colors.red)
      )
    ];

    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Title Here'),
      ),
      body: new Container(
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Flexible(
                child: new FlutterMap(
                  options: new MapOptions(
                    center: new LatLng(41.8781, -87.6298),
                    zoom: 8.0
                  ),
                  layers: [
                    new TileLayerOptions(
                      urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                      subdomains: ['a', 'b', 'c']
                    ),
                    new MarkerLayerOptions(
                      markers: markers
                    )
                  ],
                )
              )
            ],
          ),
        ),
      ),
    );
  }
}