import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  void _launch(BuildContext context, String urlString) async {
    if (await canLaunch(urlString)) {
      await launch(urlString);
    } else {
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
          title: new Text('Failed'),
          content: new Text('Failed to open url'),
          actions: <Widget>[
            new TextButton(
              onPressed: () => Navigator.pop(context),
              child: new Text('OK')
            )
          ],
        )
      );
    }
  }
  

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Title Here'),
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new ElevatedButton(onPressed: () => _launch(context, 'https://arisca-abdullah.github.io'), child: new Text('URL')),
              new ElevatedButton(onPressed: () => _launch(context, 'mailto:arisca.abdullah08@gmail.com'), child: new Text('Email')),
              new ElevatedButton(onPressed: () => _launch(context, 'sms:085156110915'), child: new Text('SMS')),
              new ElevatedButton(onPressed: () => _launch(context, 'tel:085156110915'), child: new Text('Telephone')),
            ],
          ),
        ),
      ),
    );
  }
}