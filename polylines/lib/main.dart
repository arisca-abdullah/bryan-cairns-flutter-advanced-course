import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late List<LatLng> points;
  late MapController mapController;
  late List<Marker> markers;

  @override
  void initState() {
    super.initState();

    mapController = new MapController();

    points = <LatLng>[
      new LatLng(41.8781, -87.6298),
      new LatLng(42.3314, -84.0458),
      new LatLng(42.7325, -84.5555),
    ];

    markers = points
      .map((LatLng point) => new Marker(
        width: 80.0,
        height: 80.0,
        point: point,
        builder: (context) => new Icon(Icons.pin_drop, color: Colors.red)
      ))
      .toList();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Title Here'),
      ),
      body: new Container(
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Flexible(
                child: new FlutterMap(
                  mapController: mapController,
                  options: new MapOptions(
                    center: new LatLng(41.8781, -87.6298),
                    zoom: 8.0
                  ),
                  layers: [
                    new TileLayerOptions(
                      urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                      subdomains: ['a', 'b', 'c']
                    ),
                    new MarkerLayerOptions(
                      markers: markers
                    ),
                    new PolylineLayerOptions(
                      polylines: [
                        new Polyline(
                          points: points,
                          strokeWidth: 4.0,
                          color: Colors.purple
                        )
                      ]
                    )
                  ],
                )
              )
            ],
          ),
        ),
      ),
    );
  }
}