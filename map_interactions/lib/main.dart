import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late MapController mapController;
  late Map<String, LatLng> coords;
  late List<Marker> markers;

  @override
  void initState() {
    super.initState();
    
    mapController = new MapController();
    
    coords = new Map<String, LatLng>();
    coords.putIfAbsent('Chicago', () => new LatLng(41.8781, -87.6290));
    coords.putIfAbsent('Detroit', () => new LatLng(42.3314, -83.0458));
    coords.putIfAbsent('Lansing', () => new LatLng(42.7325, -84.5555));

    markers = <Marker>[];
    coords.forEach((key, value) {
      markers.add(
        new Marker(
          width: 80.0,
          height: 80.0,
          point: value,
          builder: (context) => new Icon(Icons.pin_drop, color: Colors.red)
        )
      );
    });
  }

  List<Widget> _makeButtons() {
    List<Widget> list = <Widget>[];

    coords.forEach((key, value) {
      list.add(
        new ElevatedButton(
          onPressed: () => mapController.move(value, 8.0),
          child: new Text(key)
        ));
    });

    return list;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Title Here'),
      ),
      body: new Container(
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Row(
                children: _makeButtons(),
              ),
              new Flexible(
                child: new FlutterMap(
                  mapController: mapController,
                  options: new MapOptions(
                    center: new LatLng(41.8781, -87.6298),
                    zoom: 8.0
                  ),
                  layers: [
                    new TileLayerOptions(
                      urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                      subdomains: ['a', 'b', 'c']
                    ),
                    new MarkerLayerOptions(
                      markers: markers
                    )
                  ],
                )
              )
            ],
          ),
        ),
      ),
    );
  }
}