import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late bool _visible;

  @override
  void initState() {
    super.initState();
    _visible = true;
  }

  void _toggleVisible() {
    setState(() => _visible = !_visible);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Title here'),
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Opacity(
                opacity: _visible ? 1.0 : 0.2,
                child: new Text('Now you see me now you don\'t'),
              ),
              new ElevatedButton(onPressed: _toggleVisible, child: new Text('Toggle'))
            ],
          ),
        ),
      ),
    );
  }
}